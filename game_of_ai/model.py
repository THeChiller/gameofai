"""Model for the agent."""
from __future__ import annotations

from pathlib import Path

import torch
from torch import Tensor
from torch.nn import Linear, Module
from torch.nn.functional import relu


class LinearQNET(Module):
    """Linear QNET as model."""

    _linear_input: Linear
    _linear_hidden_1: Linear
    _linear_hidden_2: Linear
    _linear_hidden_3: Linear

    def __init__(self: LinearQNET, input_size: int, hidden_size: int, output_size: int) -> None:
        """Create an instance of the class."""
        super().__init__()
        self._linear_input = Linear(input_size, hidden_size)
        self._linear_hidden_1 = Linear(hidden_size, hidden_size)
        self._linear_hidden_2 = Linear(hidden_size, hidden_size)
        self._linear_hidden_3 = Linear(hidden_size, output_size)

    def forward(self: LinearQNET, x: Tensor) -> Tensor:
        """Progress a tensor through the layers to make a prediction."""
        x = relu(self._linear_input(x))
        x = self._linear_hidden_1(x)
        x = self._linear_hidden_2(x)
        x = self._linear_hidden_3(x)
        return x

    def save(self: LinearQNET, path: Path) -> None:
        """Store the model on disk."""
        if not path.is_dir():
            path.mkdir(exist_ok=True, parents=True)
        file_path = path / "goai_model.pth"
        torch.save(self.state_dict(), file_path)
