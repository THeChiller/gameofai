"""Model for the agent."""
from __future__ import annotations

import numpy
import torch
from numpy import ndarray
from torch import Tensor, optim, tensor, unsqueeze
from torch.nn import MSELoss
from torch.optim import Optimizer

from game_of_ai.config import Configuration
from game_of_ai.model import LinearQNET


class QTrainer:
    """Linear QNET as model."""

    _model: LinearQNET
    _learning_rate: float
    _gamma: float
    _optimizer: Optimizer
    _criterion: MSELoss
    _config: Configuration

    def __init__(self: QTrainer, model: LinearQNET, config: Configuration) -> None:
        """Create an instance of the class."""
        self._config = config
        self._gamma = config.GAMMA
        self._learning_rate = config.LEARNING_RATE
        self._model = model
        self._optimizer = optim.Adam(model.parameters(), lr=self._learning_rate)
        self._criterion = MSELoss()

    def train_step(
        self: QTrainer, state_old: ndarray, action: ndarray, reward: float, state_new: ndarray, done: bool
    ) -> None:
        """Train the model."""
        state = unsqueeze(tensor(state_old, dtype=torch.float), 0)
        next_state = unsqueeze(tensor(state_new, dtype=torch.float), 0)
        action = unsqueeze(tensor(action, dtype=torch.long), 0)
        reward = unsqueeze(tensor(reward, dtype=torch.float), 0)
        dones = (done,)
        self._train(state=state, next_state=next_state, action=action, reward=reward, done=dones)

    def train_steps(
        self: QTrainer,
        states_old: list[ndarray],
        actions: list[ndarray],
        rewards: list[float],
        states_new: list[ndarray],
        dones: tuple[bool],
    ) -> None:
        """Train the model."""
        state = tensor(numpy.array(states_old), dtype=torch.float)
        next_state = tensor(numpy.array(states_new), dtype=torch.float)
        action = tensor(numpy.array(actions), dtype=torch.long)
        reward = tensor(numpy.array(rewards), dtype=torch.float)
        self._train(state=state, next_state=next_state, action=action, reward=reward, done=dones)

    def _train(
        self: QTrainer, state: Tensor, next_state: Tensor, action: Tensor, reward: Tensor, done: tuple[bool]
    ) -> None:
        prediction: Tensor = self._model(state)

        target = prediction.clone()
        for index in range(len(done)):
            q_new = reward[index]
            if not done[index]:
                q_new = reward[index] + self._gamma * torch.max(self._model(next_state[index]))
            pred = torch.greater_equal(action[index], self._config.PREDICTION_MIN_CERTAINTY)
            for sub_index in range(action[index].shape[1]):
                if pred[0, sub_index] is True:
                    target[index][sub_index] = q_new

        self._optimizer.zero_grad()
        loss = self._criterion(target, prediction)
        loss.backward()

        self._optimizer.step()
