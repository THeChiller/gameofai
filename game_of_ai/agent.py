"""Agent to train."""
from __future__ import annotations

import logging
import random
from collections import deque
from time import time

import numpy
import torch
from numpy import ndarray
from torch import Tensor, tensor

from .config import Configuration
from .game_of_life import GameOfLife
from .model import LinearQNET
from .trainer import QTrainer
from .util import plot


class Agent:
    """Agent to train."""

    _config: Configuration
    _game: GameOfLife
    _game_count: int
    _epsilon: float
    _gamma: float
    _learning_rate: float
    _memory: deque
    _model: LinearQNET
    _trainer: QTrainer
    _plot_scores: list
    _plot_mean_scores: list
    _plot_mean_rewards: list
    _total_score: float
    _record: float
    # _reward_sum: float
    _start_time: float

    def __init__(self: Agent, config: Configuration) -> None:
        """Create an instance of the class."""
        self._config = config
        self._game = GameOfLife(config=config, instance_id=0)
        self._game_count = 0
        self._epsilon = 0
        self._gamma = config.GAMMA
        self._learning_rate = config.LEARNING_RATE
        self._memory = deque(maxlen=config.MAX_MEMORIES)
        self._plot_scores = []
        self._plot_mean_scores = []
        self._plot_mean_rewards = []
        self._total_score = 0
        self._record = 0
        self._model = LinearQNET(
            input_size=config.HEIGHT_FIELDS * config.WIDTH_FIELDS,
            hidden_size=config.HIDDEN_LAYER_SIZE,
            output_size=config.HEIGHT_FIELDS * config.WIDTH_FIELDS,
        )
        self._trainer = QTrainer(model=self._model, config=config)
        # self._reward_sum = 0
        self._start_time = 0

    def remember(
        self: Agent, state_old: ndarray, action: ndarray, reward: float, state_new: ndarray, done: bool
    ) -> None:
        """Remember an action."""
        self._memory.append((state_old, action, reward, state_new, done))

    def train_short_memory(
        self: Agent, state_old: ndarray, action: ndarray, reward: float, state_new: ndarray, done: bool
    ) -> None:
        """Train a long memory."""
        self._trainer.train_step(state_old=state_old, action=action, reward=reward, state_new=state_new, done=done)

    def train_long_memory(self: Agent) -> None:
        """Train a long memory."""
        if len(self._memory) > self._config.BATCH_SIZE:
            mini_sample = random.sample(self._memory, self._config.BATCH_SIZE)
        else:
            mini_sample = self._memory

        states_old, actions, rewards, states_new, dones = zip(*mini_sample, strict=False)
        self._trainer.train_steps(
            states_old=states_old, actions=actions, rewards=rewards, states_new=states_new, dones=dones
        )

    def get_action(self: Agent, state: ndarray) -> ndarray:
        """Get an action."""
        self._epsilon = self._config.MAX_RANDOM_ACTIONS - self._game_count

        if self._epsilon > 0 and random.randint(0, int(self._config.MAX_RANDOM_ACTIONS * 2.5)) < self._epsilon:
            # make random move
            logging.debug("random action")
            number_of_cells_to_place = random.randint(0, self._config.MAX_CELLS_TO_PLACE)
            selected_cells = random.sample(
                list(range(0, self._config.WIDTH_FIELDS * self._config.HEIGHT_FIELDS)), number_of_cells_to_place
            )
            action = numpy.zeros((1, self._config.HEIGHT_FIELDS * self._config.WIDTH_FIELDS))
            for index in selected_cells:
                action[0, index] = 1
        else:
            # make predicted move
            logging.debug("predicted action")
            state0 = tensor(state, dtype=torch.float)
            prediction: Tensor = self._model(state0)
            prediction = torch.greater_equal(prediction, self._config.PREDICTION_MIN_CERTAINTY)
            action = numpy.zeros((1, self._config.HEIGHT_FIELDS * self._config.WIDTH_FIELDS))
            for index in range(action.shape[1]):
                if prediction[index]:
                    action[0, index] = 1

        return action

    def train(self: Agent) -> None:
        """Train the agent."""
        self._start_time = time()
        while True:
            state_old = self._game.get_state()
            action = self.get_action(state=state_old)

            reward, done, score = self._game.step(action=action)
            state_new = self._game.get_state()
            logging.debug("Reward:", "%.2f" % reward)
            # self._reward_sum += reward

            self.train_short_memory(state_old=state_old, action=action, reward=reward, state_new=state_new, done=done)

            self.remember(state_old=state_old, action=action, reward=reward, state_new=state_new, done=done)

            if done:
                # mean_reward = self._reward_sum / self._game._cycle
                # self._reward_sum = 0
                self._game.reset()
                self._game_count += 1
                self.train_long_memory()
                if score > self._record:
                    self._record = score
                    self._model.save(self._config.STORAGE_PATH)

                self._plot_scores.append(score)
                self._total_score += score
                mean_score = self._total_score / self._game_count
                self._plot_mean_scores.append(mean_score)
                self._plot_mean_rewards.append(reward)
                plot(scores=self._plot_scores, mean_scores=self._plot_mean_scores, mean_rewards=self._plot_mean_rewards)

                current_time = time()
                print(
                    "Game",
                    self._game_count,
                    "Score",
                    "%.2f" % score,
                    "Avg. Reward",
                    "%.2f" % reward,
                    "Record",
                    "%.2f" % self._record,
                    "Avg. SPG",
                    "%.2f" % ((current_time - self._start_time) / self._game_count),
                )
