"""CLI for Game of AI."""

import asyncio
import logging
import pathlib
from functools import wraps
from typing import Callable

import click

from game_of_ai.agent import Agent
from game_of_ai.config import Configuration, get_config
from game_of_ai.game_of_life import GameOfLife

config = get_config()


def async_cmd(func: Callable) -> Callable:
    """Decorator to provide async click functions."""

    @wraps(func)
    def wrapper(*args: tuple, **kwargs: dict) -> None:
        """Wrapper function of the decorator."""
        return asyncio.run(func(*args, **kwargs))

    return wrapper


@click.group()
@click.option(
    "--env-file",
    "-e",
    type=click.Path(exists=True, dir_okay=False, path_type=pathlib.Path),
    help="Specify a .env file to load as settings.",
)
def cli(env_file: pathlib.Path) -> None:
    """Start the Game of AI.

    For configuring the application you have multiple options:

    - You can set environment variables, which will overwrite the default settings.

    - You can use a .env file, which will overwrite the default settings and
      environment variables.

    Args:
        env_file (Path):
            The path to a .env file to load.
    """
    # NOTE that click takes above documentation for generating help text
    # Thus the documentation refers to the application per se and not the
    # function (as it should)
    if env_file is not None:
        global config  # pylint: disable =W0603,C0103
        config = Configuration(_env_file=env_file)
    logging.basicConfig(level=config.LOG_LEVEL)


@cli.command()
def configuration() -> None:
    """Print the configuration in .env style."""
    print(config.get_env())


@click.option(
    "--manually",
    "-m",
    is_flag=True,
    type=click.BOOL,
    help="Normal manual game.",
)
@cli.command()
def gol(manually: bool) -> None:
    """Start Game of AI."""
    if manually:
        gol = GameOfLife(config=config, instance_id=0)
        gol.run_manually()
    else:
        agent = Agent(config=config)
        agent.train()
