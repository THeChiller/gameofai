"""Configuration for Game of AI."""
from __future__ import annotations

import logging
from pathlib import Path

from pydantic.color import Color
from pydantic_settings import BaseSettings, SettingsConfigDict

_LOADED_CONFIG = None


class Configuration(BaseSettings):
    """Configartion for Game of AI."""

    # logging
    LOG_LEVEL: int = logging.INFO

    # GOL config
    SQUARE_SIZE: int = 10
    WIDTH_FIELDS: int = 40
    HEIGHT_FIELDS: int = 30
    COLOR_GRID: Color = Color((40, 40, 40))
    COLOR_CELL_DEAD: Color = Color((10, 10, 10))
    COLOR_CELL_ALIVE: Color = Color((255, 255, 255))
    TARGET_UPDATES_PERS_SECOND: int = 100000
    PRINT_STATISTICS: bool = False
    HEADLESS: bool = False
    MAX_CYCLES: int = 1000

    # agent config
    MAX_MEMORIES: int = 100000
    BATCH_SIZE: int = 500
    MAX_CELLS_TO_PLACE: int = 30 * 40
    PREDICTION_MIN_CERTAINTY: float = 0.01
    MAX_RANDOM_ACTIONS: int = 1000

    # model config
    STORAGE_PATH: Path = Path(__file__).parent.parent / "models"
    LEARNING_RATE: float = 0.001
    GAMMA: float = 0.9
    HIDDEN_LAYER_SIZE: int = 1024

    def get_env(self: Configuration) -> str:
        """Print the current settings in .env style."""
        settings_dict = self.__dict__

        settings_str = ""

        for key, value in settings_dict.items():
            if isinstance(value, list):
                settings_str += f"{self.model_config['env_prefix']}{key}=["
                settings_str += ",".join([f'"{str(val)}"' for val in value])

                settings_str += "]\n"
            else:
                settings_str += f"{self.model_config['env_prefix']}{key}={value}\n"

        return settings_str

    model_config = SettingsConfigDict(case_sensitive=True, env_prefix="")


def get_config(fresh_load: bool = False) -> Configuration:
    """Get a set-up config object.

    Returns:
        Configuration: The initialized config.
    """
    global _LOADED_CONFIG  # pylint: disable=W0603
    if not _LOADED_CONFIG or fresh_load:
        _LOADED_CONFIG = Configuration()

    return _LOADED_CONFIG
