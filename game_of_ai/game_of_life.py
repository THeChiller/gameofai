"""Game of Life simulation."""
from __future__ import annotations

import logging
from time import sleep, time

import pygame
from numpy import ndarray, ndindex, sum, zeros
from pygame import Surface

from game_of_ai.config import Configuration


class GameOfLife:
    """Class running Game of Lfe."""

    _instance_id: int
    _config: Configuration
    _screen: Surface
    _shown_cells: list[ndarray]
    _next_cells: ndarray
    _target_sleep_s: float
    progress: bool
    _last_cycle_time: float
    _max_living_percent: float
    _cycle: int
    _cycles_left: int
    _rolling_average_living_percent: float
    _set_tiles: int
    # _initial_set_of_cells: bool
    _headless: bool

    def __init__(self: GameOfLife, config: Configuration, instance_id: int) -> None:
        """Create an instance of the class."""
        self._config = config
        self._instance_id = instance_id
        pygame.init()
        self._target_sleep_s = 1 / self._config.TARGET_UPDATES_PERS_SECOND
        self._screen = pygame.display.set_mode(
            (
                self._config.WIDTH_FIELDS * self._config.SQUARE_SIZE,
                self._config.HEIGHT_FIELDS * self._config.SQUARE_SIZE,
            )
        )
        self._headless = config.HEADLESS

        self.reset()

    def reset(self: GameOfLife) -> None:
        """Reset the game state."""
        self._shown_cells = [
            zeros((self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS)),
            zeros((self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS)),
            zeros((self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS)),
            zeros((self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS)),
        ]
        self._screen.fill(self._config.COLOR_GRID.as_rgb_tuple())
        self.progress = False
        self._last_cycle_time = 0
        self._max_living_percent = 0
        self._cycle = 0
        self._cycles_left = self._config.MAX_CYCLES
        self._rolling_average_living_percent = 0
        self._set_tiles = 0
        # self._initial_set_of_cells = True

    def _build_next_cells(self: GameOfLife) -> None:
        """Update the game board."""
        logging.debug("building next cells")
        # all cells dead
        self._next_cells = zeros((self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS))

        for row, column in ndindex(self._shown_cells[0].shape):
            alive_neighbours = (
                sum(self._shown_cells[0][row - 1 : row + 2, column - 1 : column + 2])
                - self._shown_cells[0][row, column]
            )

            if self._shown_cells[0][row, column] == 1 and 2 <= alive_neighbours <= 3:
                # stays alive
                self._next_cells[row, column] = 1
            elif alive_neighbours == 3:
                # becomes alive
                self._next_cells[row, column] = 1

    def _update_screen(self: GameOfLife) -> None:
        """Update the screen/."""
        logging.debug("updating screen")
        self._shown_cells.insert(0, self._next_cells)
        self._shown_cells.pop()
        if not self._headless:
            self._screen.fill(self._config.COLOR_GRID.as_rgb_tuple())

            for row, column in ndindex(self._shown_cells[0].shape):
                if self._shown_cells[0][row, column] == 1:
                    color = self._config.COLOR_CELL_ALIVE
                else:
                    color = self._config.COLOR_CELL_DEAD
                pygame.draw.rect(
                    self._screen,
                    color.as_rgb_tuple(),
                    (
                        column * self._config.SQUARE_SIZE,
                        row * self._config.SQUARE_SIZE,
                        self._config.SQUARE_SIZE - 1,
                        self._config.SQUARE_SIZE - 1,
                    ),
                )
            pygame.display.update()

    def toggle_progress(self: GameOfLife) -> bool:
        """Toggle, if the game is progression.

        Returns:
            bool: The updated progress state.
        """
        self.progress = not self.progress
        return self.progress

    def set_cell(self: GameOfLife, x: int, y: int) -> None:
        """Set a cell to living.

        Args:
            x: The x coordinate (width).
            y: The y coordinate (height).
        """
        logging.debug("setting cell %s/%s", x, y)
        self._shown_cells[0][y, x] = 1

    def get_living_percent(self: GameOfLife) -> float:
        """Calculate the percent of living cells.

        Returns:
            float: percent of living cells
        """
        return (
            100
            * sum(self._shown_cells[0][0 : self._config.HEIGHT_FIELDS - 1, 0 : self._config.WIDTH_FIELDS - 1])
            / (self._config.WIDTH_FIELDS * self._config.HEIGHT_FIELDS)
        )

    def _update_rolling_average_living_percent(self: GameOfLife, living_percent: float) -> None:
        """Update the rolling average."""
        self._rolling_average_living_percent = (
            self._rolling_average_living_percent * (self._cycle - 1) + living_percent
        ) / self._cycle

    def _print_statistics(self: GameOfLife, living_percent: float) -> None:
        """Print state statistics."""
        print("####################")
        print(f"instance id                   : {self._instance_id}")
        print(f"cycle                         : {self._cycle}")
        print(f"cycles left                   : {self._cycles_left}")
        print(f"cycle runtime                 : {'%.0f' % (self._last_cycle_time * 1000)}ms")
        print(f"living percent                : {'%.2f' % living_percent}%")
        print(f"rolling average living percent: {'%.2f' % self._rolling_average_living_percent}%")
        print(f"max living percent            : {'%.2f' % self._max_living_percent}%")

    def _game_over(self: GameOfLife) -> bool:
        """Determine if one is game over."""
        return not bool(self._shown_cells[0].sum())
        # if self._cycle < 4:
        #     return False
        # if array_equal(self._shown_cells[0], self._shown_cells[1]):
        #     return True
        # if array_equal(self._shown_cells[0], self._shown_cells[2]):
        #     return True
        # if array_equal(self._shown_cells[0], self._shown_cells[3]):
        #     return True
        # return False

    def _run_game_step(self: GameOfLife) -> None:
        """Run the game until game over."""
        start_time = time()
        self._cycle += 1
        self._build_next_cells()
        self._update_screen()
        living_percent = self.get_living_percent()
        if living_percent > self._max_living_percent:
            self._max_living_percent = living_percent
        self._update_rolling_average_living_percent(living_percent=living_percent)
        self._last_cycle_time = time() - start_time

        if self._config.PRINT_STATISTICS:
            self._print_statistics(living_percent=living_percent)
        sleep(max(0, self._target_sleep_s - time() - start_time))

    def run_manually(self: GameOfLife) -> None:
        """Run the game."""
        self._build_next_cells()
        pygame.display.flip()
        pygame.display.update()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    logging.debug("quitting")
                    pygame.quit()
                    return
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        logging.debug("toggling progress")
                        self.toggle_progress()

                if pygame.mouse.get_pressed()[0]:
                    logging.debug("placing cell")
                    mouse_position = pygame.mouse.get_pos()
                    self.set_cell(
                        x=mouse_position[0] // self._config.SQUARE_SIZE,
                        y=mouse_position[1] // self._config.SQUARE_SIZE,
                    )

            if self.progress:
                self._run_game_step()
                if self._game_over():
                    return
            else:
                self._update_screen()

    def step(self: GameOfLife, action: ndarray) -> tuple[float, bool, float]:
        """Play one step of the game."""
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self._headless = not self._headless

        action = action.reshape(self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS)
        if action.shape != (self._config.HEIGHT_FIELDS, self._config.WIDTH_FIELDS):
            raise RuntimeError(f"invalid action: {action.shape}")
        for row, column in ndindex(action.shape):
            if action[row, column] == 1:
                self.set_cell(x=column, y=row)
        cells_placed = action.sum()
        logging.debug(f"placed {cells_placed} cells")
        while True:
            self._run_game_step()
            done = False
            game_over = self._game_over()
            if game_over:
                done = True
            if self._config.MAX_CYCLES > 0:
                self._cycles_left -= 1
                if self._cycles_left == 0:
                    done = True
            # if done:
            score = self._rolling_average_living_percent
            if game_over:
                reward = -10
            else:
                reward = score # self._cycle* score / 100
                # reward = (
                #     self._rolling_average_living_percent
                #     - self.get_living_percent()
                #     - int(self._initial_set_of_cells)
                #     * cells_placed
                #     / (self._config.WIDTH_FIELDS * self._config.HEIGHT_FIELDS)
                # )
                # self._initial_set_of_cells = False
            if done:
                break
        return reward, done, score

    def get_state(self: GameOfLife) -> ndarray:
        """Get the current game state."""
        return self._shown_cells[0].flatten()
