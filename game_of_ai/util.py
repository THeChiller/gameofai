"""Helpers."""

import matplotlib.pyplot as plt
from IPython import display

plt.ion()


def plot(scores: list[float], mean_scores: list[float], mean_rewards: list[float]) -> None:
    """Plot scores."""
    display.clear_output(wait=True)
    display.display(plt.gcf())
    plt.clf()
    plt.title("Training...")
    plt.xlabel("Number of Games")
    plt.ylabel("Score")
    plt.plot(scores, color="yellow")
    plt.plot(mean_scores, color="red")
    # plt.plot(mean_rewards, color="blue")
    plt.ylim(ymin=0)
    plt.text(len(scores) - 2, scores[-1], f"Game Score: {'%.2f' % scores[-1]}")
    plt.text(len(mean_scores) - -2, mean_scores[-1], f"Mean Score: {'%.2f' % mean_scores[-1]}")
    # plt.text(len(mean_rewards) - -2, mean_rewards[-1], f"Mean Reward: {'%.2f' % mean_rewards[-1]}")
    plt.show(block=False)
    plt.pause(0.1)
