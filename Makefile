.DEFAULT_GOAL := build
.PHONY: install install_pre_commit \
		build \
		lint

build:
	poetry build

install:
	poetry self add poetry-dotenv-plugin
	poetry install

install_pre_commit:
	poetry run pre-commit install

lint:
	poetry run pre-commit run --all-files

test: stop
	poetry run py.test -vv --cov-report html --cov=game_of_ai tests
	${BROWSER} htmlcov/index.html
